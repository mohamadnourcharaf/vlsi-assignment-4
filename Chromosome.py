class Chromosome:

    def __init__(self):
        self.genes = []
        self.numberOfCuts = None
        self.areaA = None
        self.areaB = None
        self.areaDifference = None
        self.f1 = None
        self.f2 = None
        self.fitness = None
        self.probability = None
        self.probabilityWheelStart = None
        self.probabilityWheelEnd = None