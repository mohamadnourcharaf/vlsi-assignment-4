import Parameters
import DataManager
import GeneticAlgorithm


def start():

    # GA on MATLAB Graph Data
    geneticAlgorithmOnMATLABGraphData(Parameters.matlabFilePath)


def geneticAlgorithmOnMATLABGraphData(matlabFilePath):

    # Get Nodes from Matlab Graph Data
    nodes = DataManager.getGraphFromMatlabFile(matlabFilePath)

    # Set Node Weights Randomly
    DataManager.setNodeWeightsRandomly(nodes,Parameters.minRandomWeight,Parameters.maxRandomWeight)

    # Partition with Genetic Algorithm
    GeneticAlgorithm.partitionWithGeneticAlgorithm(nodes)


start()
