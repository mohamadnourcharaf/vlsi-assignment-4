# Parameters

# MATLAB Graph Data Filepath
add20MatlabFilePath = "add20.mat"
add32MatlabFilePath = "add32.mat"
threeEltMatlabFilePath = "3elt.mat"
fourEltMatlabFilePath = "barth5.mat"

matlabFilePath = "RANDOM" # To generate a Random Graph, set this field to any random string

# Random Graph Parameters
randomGraphNodeSize = 100
probabilityEdgeCreation = 0.5

# Random Weights Parameters
minRandomWeight = 1
maxRandomWeight = 20

# Genetic Algorithm Parameters
safetyFactorForMaxNumberOfCuts = 1.2

W1 = 0.5  # Min-Cut Objective Weight
W2 = 0.5  # Balance Objective Weight

populationSize = 50
numberOfGenerations = 100
elitePercentage = 0.1
survivingPercentage = 0.4
probabilityIncrement = 0.01
maxCrossoverTries = 20
mutationProbability = 0.1  # For uniform mutation (probability = 1/numberOfGenes), set this value to zero
