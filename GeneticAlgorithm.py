import matplotlib.pyplot as plt

import random
import Parameters
import Chromosome

elitePopulationSize = int(Parameters.elitePercentage * Parameters.populationSize)
offspringPopulationSize = int(Parameters.populationSize - elitePopulationSize)
survivingPopulationSize = int(Parameters.survivingPercentage * Parameters.populationSize)

bestChromosomes = []


def partitionWithGeneticAlgorithm(nodes):

    # Initialize Population, each chromosome will have its nodes partitioned randomly
    chromosomes = initializePopulation(nodes)

    # Calculate Population Number of Cuts and Areas
    calculatePopulationNumberOfCutsAndAreas(chromosomes,nodes)

    # Get Max possible number of cuts using approximation and a safety factor
    maxNumberOfCuts = max(chromosomes, key=lambda chromosome: chromosome.numberOfCuts).numberOfCuts * Parameters.safetyFactorForMaxNumberOfCuts

    # Get Max possible area difference
    totalArea = chromosomes[0].areaA + chromosomes[0].areaB
    maxAreaDifference = totalArea

    # Calculate Population Fitness
    calculatePopulationFitness(chromosomes, nodes, maxNumberOfCuts, maxAreaDifference)

    # Sort in Decreasing Order
    sortChromosomes(chromosomes)

    # Calculate Population Probabilities
    calculatePopulationProbabilities(chromosomes)

    # Process Generations
    processGenerations(chromosomes, nodes, maxNumberOfCuts, maxAreaDifference)

    # Plots
    generatePlots(bestChromosomes)


def generatePlots(chromosomes):
    generations = []
    numberOfCutsVariation = []
    areaDifferenceVariation = []
    fitnessVariation = []
    for i in range(0, len(chromosomes)):
        chromosome = chromosomes[i]
        generations.append(i)
        numberOfCutsVariation.append(chromosome.numberOfCuts)
        areaDifferenceVariation.append(chromosome.areaDifference)
        fitnessVariation.append(chromosome.fitness)

    xLabel = "Generation Number"
    yArray = [numberOfCutsVariation,areaDifferenceVariation,fitnessVariation]
    yLabelArray = ["Number of Cuts", "Area A-B Difference", "Fitness"]

    for i in range(0,len(yArray)):
        y = yArray[i]
        yLabel = yLabelArray[i]
        title = yLabel + " vs " + xLabel
        plt.plot(generations, y)
        plt.title = title
        plt.xlabel(xLabel)
        plt.ylabel(yLabel)
        plt.savefig("plots/" + str(yLabel) + ".png")
        plt.close()


def processGeneration(chromosomes, nodes, generationNumber, maxNumberOfCuts, maxAreaDifference):

    # Elitism: Select Elite Population
    eliteChromosomes = selectElitePopulation(chromosomes)

    # Selection: Select Surviving Population: Roulette Wheel Selection
    survivingChromosomes = selectSurvivingPopulation(chromosomes)

    # Crossover: Crossover Surviving Population
    offspringChromosomes = crossover(survivingChromosomes)

    # Mutation: Mutate Offspring Chromosomes
    mutation(offspringChromosomes, nodes)

    # Replacement: Replace existing population with elite population and offspring population
    chromosomes.clear()
    for c in eliteChromosomes:
        chromosomes.append(c)
    for c in offspringChromosomes:
        chromosomes.append(c)

    # Calculate Population Number of Cuts and Areas
    calculatePopulationNumberOfCutsAndAreas(chromosomes,nodes)

    # Calculate Population Fitness
    calculatePopulationFitness(chromosomes, nodes, maxNumberOfCuts, maxAreaDifference)

    # Sort in Decreasing Order
    sortChromosomes(chromosomes)

    # Calculate Population Probabilities
    calculatePopulationProbabilities(chromosomes)

    # Keep Track of Best Chromosome
    bestChromosome = chromosomes[0]
    bestChromosomes.append(bestChromosome)

    # Log Result of Generations Best Chromosome
    logResult(bestChromosome, nodes, generationNumber)

    # Check for Solution Improvement and for Errors
    if generationNumber > 0:
        currentBestFitness = bestChromosome.fitness
        previousBestFitness = bestChromosomes[generationNumber - 1].fitness
        if currentBestFitness > previousBestFitness:
            print("Solution is improving!")
        elif currentBestFitness < previousBestFitness:
            print("Error: Something went wrong, solution can only improve")
            exit(0)


def logResult(chromosome, nodes, generationNumber):
    print("Generation Number: " + str(generationNumber))
    print("Number of Cuts: " + str(chromosome.numberOfCuts))
    print("Area Difference: " + str(chromosome.areaDifference) + " Area A: " + str(chromosome.areaA) + " Area B: " + str(chromosome.areaB))
    print("F1: " + str(chromosome.f1) + " F2: " + str(chromosome.f2))
    print("Fitness: " + str(chromosome.fitness))
    print("Partition A:")
    for i in range(0, len(chromosome.genes)):
        if chromosome.genes[i] == 0:
            print(str(i+1) + "(" + str(nodes[i].weight) + ") ", end="")
    print("")
    print("Partition B:")
    for i in range(0, len(chromosome.genes)):
        if chromosome.genes[i] == 1:
            print(str(i+1) + "(" + str(nodes[i].weight) + ") ", end="")
    print("")
    print("****************************************************************************************************")


def mutation(chromosomes,nodes):
    mutationProbability = 1/(len(nodes)) if Parameters.mutationProbability == 0 else Parameters.mutationProbability
    for chromosome in chromosomes:
        for i in range(0, len(chromosome.genes)):
            r = random.uniform(0, 1)
            if mutationProbability >= r:
                chromosome.genes[i] = 0 if random.uniform(0, 1) < 0.5 else 1


def crossover(chromosomes):

    offspringChromosomes = []

    while 1:

        # Pick parent 1 randomly
        parent1 = random.choice(chromosomes)

        # Pick parent 2 randomly, such that it is not the same as parent 1
        parent2 = None
        genesAreTheSame = False
        for i in range(0,Parameters.maxCrossoverTries):
            parent2 = random.choice(chromosomes)

            genesAreTheSame = compareGenes(parent1,parent2)
            if not genesAreTheSame:
                break

        if genesAreTheSame:
            print("Genes are the same")
            # If after max crossover tries genes are still the same, randomise genes of parent 2
            for i in range(0,len(parent2.genes)):
                parent2.genes[i] = 0 if random.uniform(0, 1) < 0.5 else 1

        # Apply Uniform Crossover
        for i in range(0, len(parent1.genes)):
            r = random.uniform(0,1)
            if r < 0.5:
                parent1.genes[i] = parent2.genes[i]
            else:
                parent2.genes[i] = parent1.genes[i]

        if len(offspringChromosomes) != offspringPopulationSize:
            offspringChromosome = copyChromosome(parent1)
            offspringChromosomes.append(offspringChromosome)
        else:
            break

        if len(offspringChromosomes) != offspringPopulationSize:
            offspringChromosome = copyChromosome(parent2)
            offspringChromosomes.append(offspringChromosome)
        else:
            break

    return offspringChromosomes


def selectSurvivingPopulation(chromosomes):

    survivingChromosomes = []
    potentialChromosomes = []

    for i in range(elitePopulationSize, len(chromosomes)):
        potentialChromosomes.append(chromosomes[i])

    for i in range(0, survivingPopulationSize):

        winnerChromosome = None

        # Select chromosomes randomly until desired size is met
        while 1:

            r = random.uniform(potentialChromosomes[0].probabilityWheelStart,potentialChromosomes[-1].probabilityWheelEnd)

            for potentialChromosome in potentialChromosomes:
                if potentialChromosome.probabilityWheelStart <= r <= potentialChromosome.probabilityWheelEnd:
                    winnerChromosome = potentialChromosome
                    break

            if winnerChromosome is not None:
                break

            # Increase Probabilities by Percentage
            print("Needs probability increment")
            nextProbabilityWheelStart = 0
            for potentialChromosome in potentialChromosomes:
                newProbability = potentialChromosome.probability + Parameters.probabilityIncrement
                potentialChromosome.probability = newProbability if newProbability < 1 else 1
                potentialChromosome.probabilityWheelStart = nextProbabilityWheelStart
                potentialChromosome.probabilityWheelEnd = potentialChromosome.probabilityWheelStart + potentialChromosome.probability
                nextProbabilityWheelStart = potentialChromosome.probabilityWheelEnd

        survivingChromosome = copyChromosome(winnerChromosome)
        survivingChromosomes.append(survivingChromosome)

    return survivingChromosomes


def selectElitePopulation(chromosomes):
    eliteChromosomes = []
    for i in range(0, elitePopulationSize):
        eliteChromosome = copyChromosome(chromosomes[i])
        eliteChromosomes.append(eliteChromosome)
    return eliteChromosomes


def processGenerations(chromosomes, nodes, maxNumberOfCuts, maxAreaDifference):
    for i in range(0, Parameters.numberOfGenerations):
        processGeneration(chromosomes, nodes, i, maxNumberOfCuts, maxAreaDifference)


def calculatePopulationProbabilities(chromosomes):
    sumOfFitnessValues = 0
    for chromosome in chromosomes:
        sumOfFitnessValues += chromosome.fitness

    nextProbabilityWheelStart = 0

    for chromosome in chromosomes:
        chromosome.probability = chromosome.fitness / sumOfFitnessValues
        chromosome.probabilityWheelStart = nextProbabilityWheelStart
        chromosome.probabilityWheelEnd = chromosome.probabilityWheelStart + chromosome.probability
        nextProbabilityWheelStart = chromosome.probabilityWheelEnd


def sortChromosomes(chromosomes):
    chromosomes.sort(key=lambda chromosome: chromosome.fitness, reverse=True)


def calculateChromosomeFitness(chromosome, maxNumberOfCuts, maxAreaDifference):

    # Minimize F1: Min-Cut Objective Function
    f1 = 1 if maxNumberOfCuts == 0 else chromosome.numberOfCuts / maxNumberOfCuts

    # Minimize F2: Balance Objective Function
    f2 = 1 if maxAreaDifference == 0 else chromosome.areaDifference / maxAreaDifference

    if f1 > 1 or f2 > 1:
        print("Error: These functions must strictly be between 0 and 1, recheck implementation (also check safety factors))")
        exit(0)

    # Maximize Fitness: This is the objective function to maximize
    fitness = - (Parameters.W1 * f1) - (Parameters.W2 * f2)

    # Save results
    chromosome.f1 = f1
    chromosome.f2 = f2
    chromosome.fitness = fitness


def calculateChromosomeNumberOfCutsAndAreas(chromosome,nodes):

    # Calculate Number of cuts for chromosome partitions
    for i in range(0, len(chromosome.genes)):
        nodes[i].partition = chromosome.genes[i]

    numberOfCuts = 0
    for i in range(0, len(nodes)):
        node = nodes[i]
        if node.partition == 0:
            for edge in node.edges:
                for edgeNode in edge.nodes:
                    if edgeNode.partition == node.partition:
                        continue
                    else:
                        numberOfCuts += 1

    # Calculate Areas A and B, and their difference
    areaA = 0
    areaB = 0
    for i in range(0, len(chromosome.genes)):
        if chromosome.genes[i] == 0:
            areaA += nodes[i].weight
        else:
            areaB += nodes[i].weight
    areaDifference = abs(areaA - areaB)

    # Save Results to Chromosome
    chromosome.numberOfCuts = numberOfCuts
    chromosome.areaA = areaA
    chromosome.areaB = areaB
    chromosome.areaDifference = areaDifference


def calculatePopulationFitness(chromosomes, nodes, maxNumberOfCuts, maxAreaDifference):
    for chromosome in chromosomes:
        calculateChromosomeFitness(chromosome, maxNumberOfCuts, maxAreaDifference)


def calculatePopulationNumberOfCutsAndAreas(chromosomes,nodes):
    for chromosome in chromosomes:
        calculateChromosomeNumberOfCutsAndAreas(chromosome,nodes)


def initializePopulation(nodes):
    chromosomes = []
    for i in range(0, Parameters.populationSize):
        chromosome = Chromosome.Chromosome()
        for _ in nodes:
            chromosome.genes.append(0 if random.uniform(0, 1) < 0.5 else 1)
        chromosomes.append(chromosome)
    return chromosomes


def copyChromosome(oldChromosome):
    newChromosome = Chromosome.Chromosome()

    for gene in oldChromosome.genes:
        newChromosome.genes.append(gene)

    newChromosome.numberOfCuts = oldChromosome.numberOfCuts
    newChromosome.areaA = oldChromosome.areaA
    newChromosome.areaB = oldChromosome.areaB
    newChromosome.areaDifference = oldChromosome.areaDifference
    newChromosome.f1 = oldChromosome.f1
    newChromosome.f2 = oldChromosome.f2
    newChromosome.fitness = oldChromosome.fitness
    newChromosome.probability = oldChromosome.probability
    newChromosome.probabilityWheelStart = oldChromosome.probabilityWheelStart
    newChromosome.probabilityWheelEnd = oldChromosome.probabilityWheelEnd

    return newChromosome


def compareGenes(parent1,parent2):
    genesAreTheSame = True
    for i in range(0,len(parent1.genes)):
        if parent1.genes[i] != parent2.genes[i]:
            genesAreTheSame = False
            break
    return genesAreTheSame